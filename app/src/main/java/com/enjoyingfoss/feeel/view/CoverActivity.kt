/*
 * This file is part of Feeel.
 *
 *     Feeel is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Feeel is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Feeel.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.enjoyingfoss.feeel.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.enjoyingfoss.feeel.R
import com.enjoyingfoss.feeel.model.Workout
import kotlinx.android.synthetic.main.activity_cover.*




/**
@author Miroslav Mazel
 */

//todo add license info
class CoverActivity : AppCompatActivity() {
    companion object {
        const val WORKOUT_KEY = "workout"
    }

    override fun onCreate(savedInstanceState: Bundle?) { //todo connect service and preload here
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cover)

        val workout = intent.getParcelableExtra(WORKOUT_KEY) as Workout
        titleTV.setText(workout.titleResource)

//        todo val editor = sharedpreferences.edit()
//        editor.putString("key", "value")
//        editor.commit()

        startExerciseButton.setOnClickListener {
            val startIntent = Intent(this, WorkoutActivity::class.java)
            startIntent.putExtra(WorkoutActivity.TTS_KEY, enableTTS.isChecked)
            startIntent.putExtra(WorkoutActivity.WORKOUT_KEY, workout)
            startActivity(startIntent)
        }
    }
    //todo consider creating service here, then just passing it onto the activity created
}